-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "transitions"

local run

-- Call setup hooks and reset the balls.

billiards.looking.setuprun = 
   function ()
      -- Call all bound hooks to setup this run.

      if run and billiards.setuprun then
	 callhooks (billiards.setuprun, run)
      end

      -- Set up the balls.

      for i, ball in ipairs(bodies.balls) do
	 ball.position = billiards.opening[i]
	 ball.velocity = {0, 0, 0}
	 ball.spin = {0, 0, 0}
      end
 
     if billiards.cuespeed then
	-- Bypass looking mode an enter aiming
	-- mode straigh away.

	bodies.observer.isaiming = true
     else	 
	 -- When a new shot begins:
	 -- Do a crossfade.

	 graph.transition = transitions.dissolve {
	    duration = 0.7
	 }

	 bodies.observer.longitude = bodies.cueball.position[1]
	 bodies.observer.latitude = bodies.cueball.position[2]
     end
   end

billiards.aiming.setupcue = 
   function ()
      if billiards.cuesetup then
	 bodies.cue.azimuth = billiards.cuesetup[1] or 0
	 bodies.cue.elevation = billiards.cuesetup[2] or 0

	 bodies.cue.sidespin = billiards.cuesetup[3] or 0
	 bodies.cue.follow = billiards.cuesetup[4] or 0
      end

      if billiards.cuespeed then
	 -- Set up the cue as specified and fire
	 -- it as soon as you enter aiming mode.

	 joints.bridge.motor = {-billiards.cuespeed,
				billiards.cueforce}
      else
	 -- Dissolve.
	 
	 graph.transition = transitions.dissolve {
	    duration = 0.7
	 }
	 
	 bodies.observer.longitude = bodies.cueball.position[1]
	 bodies.observer.latitude = bodies.cueball.position[2]
	 bodies.observer.radius = 0.4
	 bodies.observer.elevation = math.pi / 2 -
	    bodies.cue.elevation -
	    math.rad (5)
      end
   end

billiards.finished.recordrun = function ()
				  -- Call the hook to record the results
				  -- once the run is finished.

				  if run then
				     if billiards.recordrun then
					callhooks (billiards.recordrun, run)
				     end

				     run = run + 1
				  else
				     run = 1
				  end
			       end

billiards.waiting.standup = function ()
   if not billiards.cuespeed then
      -- Zoom out once you've taken a shot.

      bodies.observer.radius = 1.5
      bodies.observer.elevation = math.pi / 3
   end
end

billiards.turning.aim = function ()
   if not billiards.cuespeed then
      -- Coordinate your head and hands when
      -- aiming.

      bodies.cue.azimuth = bodies.observer.azimuth
   end
end
