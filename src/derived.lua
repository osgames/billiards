-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.


local field = 0

derived = setmetatable ({}, {
		 __index = function (table, key)
		    if key == "field" then
		       local x = math.abs(graphics.perspective[4])
		       local y = math.abs(graphics.perspective[5])
		       
		       return 2 * math.deg(math.atan2(x, y))
		    elseif key == "range" then
		       return {graphics.perspective[5],
			       graphics.perspective[6]}
		    elseif key == "width" then
		       return graphics.window[1]
		    elseif key == "height" then
		       return graphics.window[2]
		    elseif key == "softness" then
		       return dynamics.tolerance[1]
		    elseif key == "stiffness" then
		       return dynamics.tolerance[2]
		    elseif key == "gee" then
		       return dynamics.gravity[3]
		    end
		 end,

		 __newindex = function (table, key, value)
                    if key == "range" then
		       local tantheta = math.tan(math.rad(moregraphics.field))
		       local a = graphics.window[1] / graphics.window[2]
		       local z = value
		       
		       graphics.perspective = {-a * z[1] * tantheta,
					       a * z[1] * tantheta,
						  -z[1] * tantheta,
					       z[1] * tantheta,
					       z[1], z[2]}
		    elseif key == "field" then
		       local tantheta = math.tan(math.rad(0.5 * value))
		       local a = graphics.window[1] / graphics.window[2]
		       local n, f = graphics.perspective[5], graphics.perspective[6]
		       
		       field = value
		       graphics.perspective = {-a * n * tantheta,
					       a * n * tantheta,
						  -n * tantheta,
					       n * tantheta,
					       n, f}
		    elseif key == "width" then
		       graphics.window = {value, graphics.window[2]}
		    elseif key == "height" then
		       graphics.window = {graphics.window[1], value}
		    elseif key == "softness" then
		       dynamics.tolerance = {value, dynamics.tolerance[2]}
		    elseif key == "stiffness" then
		       dynamics.tolerance = {dynamics.tolerance[1], value}
		    elseif key == "gee" then
		       dynamics.gravity = {0, 0, value}
 		    end
		 end
})
