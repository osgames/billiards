<!--
    Copyright (C) 2008 Papavasileiou Dimitris                             
                                                                     
    This program is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation, either version 3 of the License, or    
    (at your option) any later version.                                  
    
    This program is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of       
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    GNU General Public License for more details.                         
                                                                     
    You should have received a copy of the GNU General Public License    
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<head>
  <title>Settings</title>
  <link rel="stylesheet" href="stylesheet">

    <style type="text/css">
      col.first {width : 12em;}
      col.second {width : 7em ;}
    </style>

  <script type="text/javascript">

    function init()
    {
       document.forms[0]['options.fullscreen'][1].onclick = 
	  function ()
	  {
	     document.forms[0]['derived.width'].disabled =
		document.forms[0]['options.fullscreen'][1].checked;
	     document.forms[0]['derived.height'].disabled =
		document.forms[0]['options.fullscreen'][1].checked;       
	  }
    }

    function submit(save)
    {
       for (i = 0 ; i < document.forms[0].length ; i += 1) {
	  var element = document.forms[0].elements[i];

	  if (element.type == "checkbox" && element.checked) {
	     document.forms[0][element.name][0].disabled = "true";
	  }
       }

       if (document.forms[0]['options.fullscreen'][1].checked) {
	  document.forms[0]['derived.width'].disabled = false;
	  document.forms[0]['derived.width'].value =
	     "<?lua return configuration.screen[1] ?>";

	  document.forms[0]['derived.height'].disabled = false;
	  document.forms[0]['derived.height'].value =
	     "<?lua return configuration.screen[2] ?>";
       }

       document.forms[0]['save'].value = save;
       document.forms[0].submit();

       return true;
    }

  </script>
</head>

<body onLoad="init();">
  <hr> 
  <h1>
    <image type="image/svg+xml" src="/logo"/>
  </h1>
  
  <a href="javascript:submit(false)">apply</a>
  &middot;
  <a href = "javascript:submit(true)">save</a>
  &middot;
  <a href = "/">back</a>
  <hr> 

  <p>
    Edit the following form and click apply to set physical
    parameters, system settings, etc.
  </p>
  
  <form action="/settings" method="post">
    <input type="hidden" name="apply" value="true"/>
    <input type="hidden" name="save" value="false"/>
    <table>

      <col class="first">
      <col class="second">
      <col class="third">

      <?lua
      require "derived"

      local html = ""
      local groups = {
	 Gear = {
	    {'billiards.tablewidth',
	     'Table width',
	     'The width of the playing area in meters.'},
	    {'billiards.tableheight',
	     'Table height',
	     'The height of the playing area in meters.'},
	    {'billiards.cushionheight',
	     'Cushion height',
	     'The height of the cushions in meters at the ' ..  
		'point where they contact the balls.'},
	    {'billiards.ballradius',
	     'Ball radius',
	     'The radius of the balls in meters.'},
	    {'billiards.ballmass',
	     'Ball mass',
	     'The weight of the balls in kilograms.'},
	    {'billiards.cuemass',
	     'Cue mass',
	     'The mass of the cue in kilograms.  Choose this ' ..
		'according to the weight of your favorite cue.'},
	    {'billiards.cueinertia',
	     'Cue inertia',
	     'This is the effective moment of inertia of the cue ' ..
		'tip around the vertical axis.  This value affects ' ..
		'the initial deflection of the path of the cue ball due ' ..
		'to left or right english, also known as the squirt effect.'},
	    {'billiards.cueforce',
	     'Cue force',
	     'The maximum force the player can exert while stroking ' ..
		'the cue in Newtons.'},
	    
	    {'billiards.strikingrestitution',
	     'Striking restitution',
	     'The coefficient of restitution between the cue tip and ' ..
		'the cue ball.'},
	    {'billiards.collidingrestitution',
	     'Colliding restitution',
	     'The coefficient of restitution between colliding balls.  ' ..
		'This defines how efficiently the balls collide with each ' ..
		'other and should usually be very close to 1.'},
	    {'billiards.bouncingrestitution',
	     'Bouncing restitution',
	     'The coefficient of restitution between balls and ' ..
		'cushions.  Higher values result in more elastic ' ..
		'and thus efficient cushions.'},
	    {'billiards.jumpingrestitution',
	     'Jumping restitution',
	     'The coefficient of restitution describing the elasticity ' ..
		'of the table bed.  Large values make it easier ' ..
		'for a ball to bounce off the table.'},
	    
	    {'billiards.staticfriction',
	     'Static friction',
	     'The coefficient of static friction between the cloth ' ..
		'and balls.  This determines the amount of friction ' ..
		'present as the ball begins to slide.'},
	    {'billiards.slidingfriction',
	     'Sliding friction',
	     'The coefficient of sliding friction between the cloth ' ..
		'and balls.  This determines how soon the ball begins to ' ..
		'roll without slipping after it has been struck by the cue.'},
	    {'billiards.rollingfriction',
	     'Rolling friction',
	     'The coefficient of rolling friction between the cloth ' ..
		'and balls.  The lower the value the longer do ' ..
		'the balls roll before coming to a stop.  Divide one ' ..
		'by this number to get the <q>table speed</q>.'},
	    {'billiards.spinningfriction',
	     'Spinning friction',
	     'The coefficient of spinning friction between the cloth ' ..
		'and balls.  This determines the amount of friction ' ..
		'present when the ball spins like a top around the ' ..
		'vertical axis.  Larger values therefore tend to make ' ..
		'the balls lose the vertical component of their ' ..
		'rotation faster.'},
	    {'billiards.strikingfriction',
	     'Striking friction',
	     'The coefficient of friction between the cue stick and ' ..
		'the cue ball.  This value determines how well the ' ..
		'cue tip grips the cue ball and therefore how easy or ' ..
		'difficult it is for you to miscue.  Large values ' ..
		'correspond to a well chalked cue while smaller ones ' ..
		'can be used to simulate a bad or worn cue.'},
	    {'billiards.bouncingfriction',
	     'Bouncing friction',
	     'The coefficient of sliding friction between the ' ..
		'balls and the cushions.'},
	    {'billiards.slowfriction',
	     'Slow friction',
	     'The coefficient of sliding friction between two ' ..
		'colliding balls.  This is the value used for <q>slow</q> ' ..
		'collisions.'},
	    {'billiards.fastfriction',
	     'Fast friction',
	     'As in <q>Slow friction</q> above, but for <q>fast</q> ' ..
		'collisions.'}
	 },

	 Graphics = {
	    {'derived.width', 'Width', 'The width of the window.'},
	    {'derived.height', 'Height', 'The height of the window.'},
	    {'derived.field', 'Field', 'The field of view.'},
	    {'options.fullscreen', 'Fullscreen mode',
	     'Hog all available screen real estate.',
	     false},
	    {'options.toon', 'Cel shading',
	     'Use a simple cel shading renderer which should run on most ' ..
	    	'GPUs out there.  Should be useful for cartoon fans ' ..
	    	'and the graphically challenged.', false},
	    {'options.framerate', 'Display framerate',
	     'Display the framerate gauge.', false},
	    {'options.noarrays', 'No vertex arrays',
	     'Advises Techne not to use vertex arrays.  Don\'t check this '..
		'unless you\'re having trouble getting the game to start.',
	     false},
	    {'options.nobufferobjects', 'No buffer objects',
	     'Advises Techne not to use buffer objects.  Likewise, should '..
		'be checked only as a workaround for buggy drivers.',
	     false},
	 },

	 Input = {
	    {'billiards.stroke',
	     'Stroking sensitivity',
	     'The mouse sensitivity when stroking the cue.'},
	    {'billiards.linear',
	     'Linear sensitivity',
	     'The mouse sensitivity when zooming in or out.'},
	    {'billiards.angular',
	     'Angular sensitivity',
	     'The mouse sensitivity when looking around.'},
	    {'billiards.finetune',
	     'Fine tuning sensitivity',
	     'The mouse sensitivity when looking around while aiming.'},
	 },
	 
	 Dynamics = {
	    {'dynamics.stepsize',
	     'Step size',
	     'The size of the simulation timestep.'},
	    {'dynamics.iterations',
	     'Iterations',
	     'The number of iterations when using the iterative ' ..
		'solver or zero to disable it.'},
	    {'dynamics.timescale',
	     'Timescale',
	     'The ratio of simulation time to real time.  Values less than ' ..
		'one result in slow motion and greater than one in fast ' ..
		'forward.'},
	    {'derived.gee',
	     'Gravity',
	     'The acceleration of gravity in meters per square second.'},
	    {'derived.softness',
	     'Softness',
	     'The global contraint tolerance, that is the amount by ' ..
		'which a constraint can be violated.  The lower the ' ..
		'the merrier but increasing it can solve stability problems'},
	    {'derived.stiffness',
	     'Stiffness',
	     'The global contraint tolerance, that is the amount of ' ..
		'accumulated constraint error that can be corrected ' ..
		'during each time step.'},
	    {'dynamics.surfacelayer',
	     'Surface layer',
	     'The depth of allowed interpenetration between two contacting ' ..
		'surfaces.  Non-zero values, although not necessarily ' ..
		'realistic prevent repeated breaking and reestablishing of ' ..
		'contacts.'},
	    {'dynamics.popvelocity',
	     'Pop velocity',
	     'The maximum velocity that can be used to pop two ' ..
		'interpenetrating bodies away from each other.  As bodies ' ..
		'shoudln\'t interpenetrate this should be infinity but ' ..
		'smaller values help prevent jittering and popping of ' ..
		'bodies in such cases.'},
	 }
      }

      local function stringify (value)
	 if type(value) == "string" then
	    return "\"" .. value .. "\""
	 elseif type(value) == "table" then
	    for i, element in ipairs (value) do
	       value[i] = stringify(element) .. ","
	    end
	  
	    return "{" .. table.concat(value) .. "}"
	 else
	    return tostring(value)
	 end
      end

      if query.apply then
	 -- Set the new values.

	 for argument, value in pairs (query) do
	    if string.find (argument, "%.") then
	       assert(loadstring (argument .. "=" .. value))()
	    end
	 end

	 if query.save == "true" then
	    local file = io.open(os.getenv ("HOME") .. "/.billiards", "w")

	    if file then
	       file:write 'require "derived"\n'

	       for name, group in pairs (groups) do
		  file:write ("\n-- " .. name .. " options\n\n")
		  
		  for _, option in ipairs (group) do
		     file:write (option[1] .. " = " .. 
				 stringify(loadstring ("return " ..
						       option[1])()) ..
			      "\n")
		  end
	       end
	 
	       file:close()
	    end
	 end
 
	 if  billiards.started then
	    -- Set up the opening according to the current
	    -- positions.

	    for i, ball in ipairs(bodies.balls) do
	       billiards.opening[i] = ball.position
	    end

	    -- Restart.
	    
	    resources.dofile "billiards/restart.lua"
	 end
      end

      for header, group in pairs(groups) do
	 html = html .. string.format([[

      <tr><th colspan="3">%s</th></tr>]], header)
	   
	 for i, row in ipairs (group) do
	    local name, label, description, default = unpack (row)
	    local value = assert (loadstring ("return " .. name))() or default

	    html = html .. string.format([[

      <tr class="%s">]], math.mod(i, 2) == 1 and "even" or "odd")

	    if type(value) == "number" then
	       html = html .. string.format([[

        <td>%s:</td>
        <td align="center">
	  <input class="number" type="text" name="%s"
		 value="%g" size="10"/>
        </td>]], label, name, value)
	    elseif type(value) == "boolean" then
	       html = html .. string.format([[

	<td align="left" colspan="2">
          <input type="hidden" name="%s" value="false" >
	  <input class="check" type="checkbox"
                 name="%s" value="true" %s > %s </input>
        </td>]], name, name, value and "checked" or "", label)
	    end

	    html = html .. string.format([[

	<td>%s</td>
      </tr>]], description)
	 end
      end

      return html
      ?>
    </table>
  </form>
  <hr> 
</body>
