<!--
    Copyright (C) 2008 Papavasileiou Dimitris                             
                                                                     
    This program is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation, either version 3 of the License, or    
    (at your option) any later version.                                  
    
    This program is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of       
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    GNU General Public License for more details.                         
                                                                     
    You should have received a copy of the GNU General Public License    
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<head>
  <title>Shots</title>
  <link rel="stylesheet" href="stylesheet">

  <style type="text/css">
    col.first {
      width: 12em;
    }

    col.second {
       width: 7em ;
    }

    .view {
       position: absolute;
       background-color: lightyellow;
       border: 1px dashed gray;
       visibility: hidden;
       color: black;
       width: 90%;
       min-height: 95%;
       left: 5%;
    }

    .view a img {
       position:relative;
       height: 95%;
       width: 95%;
       left: 2.5%;
       top: 0;
       border-width: 0;
       padding: 2px;
    }

    .view a {
       position:relative;
    }
  </style>
</head>

<body>
  <hr> 
  <h1>
    <image type="image/svg+xml" src="/logo"/>
  </h1>
  
  <a href = "/">back</a>
  <hr> 

  <p>
    <?lua 
      return [[So far, <span id="counter">no</span> shots have been made.<span id="save">  You can <a href="#archive" onclick="save(true, this)">archive</a> this game or, alternatively <a href="#export" onclick="save(false, this)">export</a> it to a file.</span>]]
    ?>
  </p>
  
  <table name="shots">
    <col class="first">
    <col class="second">
    <col class="third">
  </table>

  <script type="text/javascript">
    var enlarged = false;
    var request, shots = <?lua return #billiards.history ?>;

    request = new XMLHttpRequest();

    function save (archive, link, shot)
    {
       var request;

       request = new XMLHttpRequest();
       link.style.backgroundColor = "#74DF00"

       request.open(archive ? "GET" : "HEAD",
		    "/" + (archive ? "archive" : "export") +
		          (shot ? "?shot=" + shot : ""), true);

       request.onreadystatechange = function() {
           if (request.readyState == 4) {
	      if (request.status == 200) {
		 link.style.backgroundColor = null
		 
		 if (!archive) {
		    window.location = "/export" + (shot ? "?shot=" + shot : "")
		 }
	      } else {
		 link.style.backgroundColor = "red"
	      }
	   }
	}
           				  
       request.send(null);
    }

    function backtrack (shot)
    {
       var request;

       request = new XMLHttpRequest();

       request.open("GET", "/backtrack?shot=" + shot, true);
       request.send(null);
    }

    function replay (shot)
    {
       var request;

       request = new XMLHttpRequest();

       request.open("GET", "/replay?shot=" + shot, true);
       request.send(null);
    }

    function enlarge (element)
    {
       if (enlarged) {
	  enlarged.style.visibility = 'hidden';
       }
	  
       enlarged = element.parentNode.getElementsByClassName('view')[0];
	  
       enlarged.style.visibility = 'visible';
       enlarged.style.top = window.pageYOffset + 0.025 * window.innerWidth;
    }

    function hide ()
    {
       enlarged.style.visibility = 'hidden';
       enlarged = null;
    }

    function update()
    {
       var table, row, cell, image, div, link, counter, span;
       var i, j;

       span = document.getElementById("counter");
       span.firstChild.nodeValue = "" + (shots > 0 ? shots : "no")

       span = document.getElementById("save");
       span.style.display = shots > 0 ? "inline" : "none";

       table = document.getElementsByTagName("table")[0];

       for (i = 1 ; table.firstChild ; i += 1) {
       	  if (table.firstChild.getElementsByTagName &&
       	      table.firstChild.getElementsByTagName("span")[0] == enlarged) {
       	     j = i;
       	  }

       	  table.removeChild(table.firstChild);
       }

       for (i = 1 ; i <= shots ; i += 1) {
         row = document.createElement("tr");
	 row.setAttribute("class", i % 2 ? "even" : "odd");
	 
	 cell = document.createElement("td");
	 cell.appendChild(document.createTextNode("" + i));
	 row.appendChild (cell);

	 cell = document.createElement("td");
	 image = document.createElement("img");
	 image.setAttribute("style", "margin-left:5px; margin-right: 5px");
	 image.setAttribute("src", "/drawtable?width=200&shot=" + i);
	 cell.appendChild (image);
	 row.appendChild (cell);

	 cell = document.createElement("td");
	 cell.setAttribute("style", "width : 100%;");
	 div = document.createElement("div");

	 link = document.createElement("a");
	 link.setAttribute("href", "#open");
	 link.setAttribute("onclick", "enlarge(this);");
	 link.appendChild (document.createTextNode("view"));
	 div.appendChild (link);

	 span = document.createElement("span");
	 span.setAttribute("class", "view");

	 link = document.createElement("a");
	 link.setAttribute("href", "#close");
	 link.setAttribute("title", "Click on the diagram to hide it");
	 link.setAttribute("onclick", "hide();");

	 image = document.createElement("img");
	 image.setAttribute("src", "/drawtable?shot=" + i);

	 link.appendChild (image);
	 span.appendChild (link);

	 div.appendChild (span);
	 
	 if (i == j) {
	     enlarge(span);
	 }

	 div.appendChild (document.createElement("br"));

	 link = document.createElement("a");
	 link.setAttribute("href", "#backtrack");
	 link.setAttribute("onclick", "backtrack(" + i + ")");
	 link.appendChild (document.createTextNode("backtrack"));
	 div.appendChild (link);

	 div.appendChild (document.createElement("br"));

	 link = document.createElement("a");
	 link.setAttribute("href", "#replay");
	 link.setAttribute("onclick", "replay(" + i + ")");
	 link.appendChild (document.createTextNode("replay"));
	 div.appendChild (link);

	 div.appendChild (document.createElement("br"));

	 link = document.createElement("a");
	 link.setAttribute("href", "#archive");
	 link.setAttribute("onclick", "save(true, this, " + i + ");");
	 link.appendChild (document.createTextNode("archive"));
	 div.appendChild (link);

	 div.appendChild (document.createElement("br"));

	 link = document.createElement("a");
	 link.setAttribute("href", "#export");
	 link.setAttribute("onclick", "save(false, this, " + i + ");");
	 link.appendChild (document.createTextNode("export"));
	 div.appendChild (link);

	 cell.appendChild (div);
	 row.appendChild (cell);

	 table.appendChild (row);
      }
    }

    function query()
    {
       request.open("GET", "/updateshot", true);
       request.onreadystatechange = check;
       request.send(null);
    }

    function check()
    {
       if (request.readyState == 4) {
	  if (request.status == 200) {
	     shots = parseInt(request.responseText);

	     update();
	     query();
	  }
       }
    }
    
    update();
    query();
  </script>
  <hr> 
</body>
