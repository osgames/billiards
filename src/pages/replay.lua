<?lua
    -- Copyright (C) 2008 Papavasileiou Dimitris                             
    --                                                                      
    -- This program is free software: you can redistribute it and/or modify 
    -- it under the terms of the GNU General Public License as published by 
    -- the Free Software Foundation, either version 3 of the License, or    
    -- (at your option) any later version.                                  
    --                                                                      
    -- This program is distributed in the hope that it will be useful,      
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of       
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    -- GNU General Public License for more details.                         
    --                                                                      
    -- You should have received a copy of the GNU General Public License    
    -- along with this program.  If not, see <http://www.gnu.org/licenses/>.

    local shot

    if not query.shot then
       return "What should I replay?"
    end

    if not bodies.observer.islooking then
       return "Can't replay while a shot is in progress."
    end

    query.shot = tonumber(query.shot)
    shot = billiards.history[query.shot]

    graph.transition = transitions.dissolve {
       duration = 0.7
    }

    for i, ball in ipairs (bodies.balls) do
       ball.position = shot[i][1]
       ball.velocity = {0, 0, 0}
       ball.spin = {0, 0, 0}

       physics.sleep (ball)
    end
	       
    graph.replayclock = frames.timer {
       period = 2,
       
       tick = function (self)
    	   billiards.looking.revert = function ()
              local sum = 0

    	      for i, ball in ipairs (bodies.balls) do
    		 local t = billiards.history[query.shot][i]

    		 if not (ball.isout or ball.ispocketed) then
    		    sum = sum + (math.distance(ball.position, t[#t]))
		    
    		    if math.distance(ball.position, t[#t]) > 1e-4 then
    		       print (i, math.distance(ball.position, t[#t]))
    		    end
    		 end
    	      end
	   
    	      print ("Total replay error: " .. sum .. " m")

    	      for i, ball in ipairs (bodies.balls) do
    		 local c

    		 c = billiards.history[#billiards.history][i]

    		 ball.position = c[#c]
    		 ball.velocity = {0, 0, 0}
    		 ball.spin = {0, 0, 0}

    		 physics.wake (ball)
    	      end

    	      billiards.looking.revert = nil
    	   end

    	   self.begin = function (self)
               for i, ball in ipairs (bodies.balls) do
		  if i == shot.initial[1] then
		     ball.position = shot.initial[2]
		     ball.velocity = shot.initial[3]
		     ball.spin = shot.initial[4]
		  end

    		  physics.wake (ball)
    	       end
	       
    	       self.parent = nil
    	    end
    	end
    }

    graph.message.text = "Replaying shot " .. query.shot .. "."

    return "Replaying shot " .. query.shot
?>
