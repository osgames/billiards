<?lua
    -- Copyright (C) 2008 Papavasileiou Dimitris                             
    --                                                                      
    -- This program is free software: you can redistribute it and/or modify 
    -- it under the terms of the GNU General Public License as published by 
    -- the Free Software Foundation, either version 3 of the License, or    
    -- (at your option) any later version.                                  
    --                                                                      
    -- This program is distributed in the hope that it will be useful,      
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of       
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    -- GNU General Public License for more details.                         
    --                                                                      
    -- You should have received a copy of the GNU General Public License    
    -- along with this program.  If not, see <http://www.gnu.org/licenses/>.

    local html = ""

    if query.shot then
       local opening

       query.shot = tonumber(query.shot)
	  
       for _ = 1, #billiards.history - query.shot + 1 do
	  opening = table.remove(billiards.history)
       end

       for i, line in ipairs (opening) do
	  billiards.opening[i] = line[1]
       end
	    
       -- Restart.

       resources.dofile "billiards/restart.lua"
       graph.message.text = "Rewound to shot " .. query.shot .. "."

       return "Rewound to shot " .. query.shot .. "."
    else
       return "Where should I backtrack to?"
    end
?>
