-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "frames"
require "transforms"
require "switches"
require "dynamics"
require "physics"
require "morejoints"
require "moremath"

local x, y, rho, theta, phi = 0, 0, 2, 0, math.rad (70)
local islooking, iswaiting, isaiming = false, true, false, false

local function adjusteye (radius, azimuth, elevation, horizontal, vertical)
   theta = math.clamp (tonumber(azimuth) or theta, -math.pi, math.pi) 
   phi = math.clamp (tonumber(elevation) or phi, 0, math.pi / 2)
   rho = math.clamp (tonumber(radius) or rho, 0, 10)
   x = math.clamp (tonumber(horizontal) or x, -5, 5)
   y = math.clamp (tonumber(vertical) or y, -5, 5)

   bodies.observer.feet.legs.waist.torso.neck.preload = rho
   bodies.observer.feet.legs.waist.preload = {-theta, 0, -phi}
      
   bodies.observer.feet.axis =
      math.normalize {bodies.observer.feet.legs.position[1] - x,
   		      bodies.observer.feet.legs.position[2] - y,
   		      0}

   bodies.observer.feet.preload = 
      math.length {bodies.observer.feet.legs.position[1] - x,
   		   bodies.observer.feet.legs.position[2] - y,
   		    0}
   
--   print (theta)

   callhooks(billiards.reorienting)
end

-- The bodies.

bodies.observer = bodies.system {
  feet = springs.linear {
      stiffness = 3000,
      damping = 1000,

      legs = bodies.point {
      	 mass = physics.spheremass (1, 0.1),
	 
	 waist = springs.euler {
	    anchor = {0, 0, billiards.ballradius},
	    axes = {{0, 0, 1}, {1, 0, 0}, {0, 1, 0}},

	    stiffness = 3000,
	    damping = 1000,

	    torso = bodies.point {
	       position = {0, 0, billiards.ballradius},
	       mass = physics.spheremass (1, 0.1),

	       neck = springs.linear {
		  stiffness = 3000,
		  damping = 1000,
		  inverted = true,

		  eye = bodies.point {
		     position = {0, 0, 2 * billiards.ballradius},
		     orientation = transforms.euler (0, 15, 0),

		     mass = physics.spheremass (1, 0.1),      
		     camera = frames.observer {}
		  },
	       },
	    },
	 },
      },
   },

   controls = switches.button {
      selected = frames.event {      
	 buttonpress = function (self, button, x, y)
	    if not bodies.observer.isaiming and
      	       button == bindings.survey then
	       bodies.observer.restore = true
		  
	       bodies.observer.oldradius = bodies.observer.radius
	       bodies.observer.oldazimuth = bodies.observer.azimuth
	       bodies.observer.oldelevation = bodies.observer.elevation

	       bodies.observer.radius = billiards.tablewidth + 0.6
	       bodies.observer.elevation = math.rad(20)	       
	       bodies.observer.longitude = 0

	       if bodies.observer.azimuth > 0 then
		  bodies.observer.latitude = 0.5 * billiards.tableheight
		  bodies.observer.azimuth = math.pi / 2
	       else
		  bodies.observer.latitude = -0.5 * billiards.tableheight
		  bodies.observer.azimuth = -math.pi / 2
	       end
	    end
	 
	    self.zero = {x, y}
	    self.motion = function (self, button, x, y)
	       dx = x - self.zero[1]
	       dy = y - self.zero[2]

	       if button == bindings.rotate then
		  local c = bodies.observer.isaiming and
		     billiards.finetune or
		     billiards.angular

		  bodies.observer.azimuth = bodies.observer.azimuth - c * dx
		  bodies.observer.elevation = bodies.observer.elevation - c * dy
	       elseif button == bindings.zoom then
		  if bodies.observer.isaiming then
		     bodies.observer.radius = 
			math.clamp(bodies.observer.radius -
				   billiards.linear * dy,
				   billiards.ballradius + 0.05,
				   0.5)
		  else
		     bodies.observer.radius =
			math.clamp(bodies.observer.radius -
				   billiards.linear * dy,
				   billiards.ballradius + 1e-2,
				   5)
		  end
	       end

	       self.zero = {x, y}
	    end
	 end,

	 buttonrelease = function (self, button)
            if bodies.observer.restore then
	       bodies.observer.radius = bodies.observer.oldradius
	       bodies.observer.azimuth = bodies.observer.oldazimuth
	       bodies.observer.elevation = bodies.observer.oldelevation
	    
	       bodies.observer.longitude = bodies.cueball.position[1]
	       bodies.observer.latitude = bodies.cueball.position[2]
	    
	       bodies.observer.restore = false
	    end

	    self.motion = nil
	 end,

	 scroll = function (self, where)
            if where == "up" then
	       dynamics.timescale = dynamics.timescale * 2
	       graph.message.text = "Speeding up to x" ..
		                    dynamics.timescale .. " speed."
	    elseif where == "down" then
	       dynamics.timescale = dynamics.timescale / 2
	       graph.message.text = "Slowing down to x" ..
                                     dynamics.timescale .. " speed."
	    end
	 end,
      }
   }
}

local observermeta = getmetatable(bodies.observer)
local observerindex = observermeta.__index
local observernewindex = observermeta.__newindex

observermeta.__index = function(table, key)
   if key == "azimuth" then
      return theta
   elseif key == "elevation" then
      return phi
   elseif key == "radius" then
      return rho
   elseif key == "longitude" then
      return x
   elseif key == "latitude" then
      return y
   elseif key == "islooking" then
      return islooking
   elseif key == "isaiming" then
      return isaiming
   elseif key == "iswaiting" then
      return iswaiting
   elseif key == "speed" then
      return math.length (bodies.observer.feet.legs.waist.torso.neck.eye.velocity)
   else
      return observerindex(table, key)
   end
end

observermeta.__newindex = function(table, key, value)
   if key == "radius" then
      adjusteye (value, theta, phi, x, y)
      callhooks(billiards.zooming)
   elseif key == "azimuth" then
      adjusteye (rho, value, phi, x, y)
      callhooks(billiards.turning)
   elseif key == "elevation" then
      adjusteye (rho, theta, value, x, y)
      callhooks(billiards.turning)
   elseif key == "longitude" then
      adjusteye (rho, theta, phi, value, y)
      callhooks(billiards.panning)
   elseif key == "latitude" then
      adjusteye (rho, theta, phi, x, value)
      callhooks(billiards.panning)
   elseif key == "islooking" and value == true then
      islooking = true
      isaiming = false
      iswaiting = false
      isfinished = false
      
      callhooks(billiards.looking)
   elseif key == "isaiming" and value == true then
      islooking = false
      isaiming = true
      iswaiting = false
      isfinished = false
      
      callhooks(billiards.aiming)
   elseif key == "iswaiting" and value == true then
      islooking = false
      isaiming = false
      iswaiting = true
      isfinished = false
      
      callhooks(billiards.waiting)
   else
      observernewindex(table, key, value)
   end
end

-- Set up a default configuration
		 
bodies.observer.longitude = 0
bodies.observer.latitude = 0
bodies.observer.radius = 1
bodies.observer.elevation = math.pi / 3
bodies.observer.azimuth = 0.1 * (math.random() - 0.5)

-- And link the node tree.

graph.observer = bodies.observer
