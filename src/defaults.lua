-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

network.http = 29176

graphics.window = {800, 600}

derived.field = 45

dynamics.stepsize = 0.0012
dynamics.ceiling = 0.1
dynamics.iterations = 0
dynamics.gravity = {0, 0, -9.81}
dynamics.timescale = 1
dynamics.surfacelayer = 1e-4
dynamics.tolerance = {1e-6, 0.2}
dynamics.popvelocity = 0.1

billiards.tablewidth = 2.84
billiards.tableheight = 1.42
billiards.cushionheight = options.pool and 0.036 or 0.037

billiards.ballradius = options.pool and 0.0286 or 0.0305
billiards.ballmass = options.pool and 0.156 or 0.210

billiards.cueradius = 5e-3
billiards.cuemass = 0.59
billiards.cueinertia = 0.0014
billiards.cueforce = 1000
billiards.cuelength = 1.44
billiards.tipoffset = 0.02

billiards.staticfriction = 0.14
billiards.slidingfriction = 0.21
billiards.rollingfriction = 0.01
billiards.spinningfriction = 0.044
billiards.strikingfriction = 0.8
billiards.slowfriction = 0.11
billiards.fastfriction = 0.01
billiards.bouncingfriction = 0.26

billiards.collidingrestitution = 0.95
billiards.strikingrestitution = 0.7
billiards.bouncingrestitution = 0.75
billiards.jumpingrestitution = 0.6

billiards.thresholds = {1e-3, 5e-2}
billiards.tracking = {0.005, 0.03}

billiards.linear = 0.02
billiards.angular = 0.007
billiards.stroke = -0.075
billiards.finetune = 0.0005

bindings.ready = 1
bindings.survey = 2
bindings.rotate = 1
bindings.move = 3
bindings.pan = 2
bindings.zoom = 3
bindings.strike = 1
bindings.elevate = 3
bindings.offset = 1
