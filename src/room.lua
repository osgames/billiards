-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "transforms"
require "resources"
require "frames"
require (options.toon and "toon" or "shading")

local w = billiards.tablewidth
local h = billiards.tableheight
local h_0 = billiards.cushionheight

local c = 0.4
local w_l = 0.5 * (c * w - 1) / (c * w / (w + 0.5))
local h_l = 0.5 * (c * w - 1) / (c * w / (h + 0.5))

-- graph.position = {0, 0, -2}
-- graph.orientation = transforms.euler (-45, 0, 0)

-- The lighting setup.

graph.lamp = options.toon and toon.lamp {
   position = {0, 0, c * w},
   orientation = transforms.euler (180, 0, 0),
  
   intensity = {[0] = 0.1, [0.04] = 0.4, [0.15] = 0.6, [0.45] = 0.8},
   ambience = 0.4
} or shading.light {
   position = {0, 0, c * w},
   orientation = transforms.euler (180, 0, 0),

   intensity = resources.clamped "billiards/imagery/light/spot.lc",
   attenuation = {1.1, 0, 0},
   
   offset = {1, 2},
   size = {2048, 1024},
   volume = {-w_l, w_l, -h_l, h_l, c * w - 1, 1 + c * w},
}

graph.ambience = not options.toon and shading.ambient {
   intensity = resources.clamped "billiards/imagery/light/roomambience.lc"
}

graph.fog = not options.toon and shading.fog {
   quadratic = 0.04,
   offset = 3,
   color = graphics.canvas
}

graph.room = bodies.environment {
   -- The floor.

   floor = not options.toon and bodies.plane {
      isfloor = true,

      position = {0, 0, -0.75},

      surface = shading.phong {
	 diffuse = resources.periodic "billiards/imagery/diffuse/parquet.lc",
	 specular = resources.periodic "billiards/imagery/specular/parquet.lc",
	 parameter = 64,
	 
	 mesh = resources.static "billiards/meshes/floor.lc"
      }
   }
}

graph.pooltable = resources.dofile "billiards/pooltable.lua"
graph.caromtable = resources.dofile "billiards/caromtable.lua"
