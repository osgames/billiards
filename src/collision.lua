-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
dynamics.collision.balls = function (a, b)
   local ball, other

   ball = a.isball and a or b
   other = a.isball and b or a

   -- The vast majority of collision will
   -- be ball-bed collisions so better get
   -- this out of the way ASAP.

   if other.isbed then
      -- Only consider the bed if we're not over a hole.
      
      if math.abs (ball.position[1]) < 0.5 * billiards.tablewidth - 0.02 or
         math.abs (ball.position[2]) < 0.5 * billiards.tableheight - 0.02 then
				 
	 local mu = billiards.slidingfriction -
	    1 / (250 * math.length (ball.velocity) + 
	      1 / (billiards.slidingfriction -
		   billiards.staticfriction))
--	 local sigma, tau = unpack(physics.spring (1500000, 750))
      
	 return 1, mu, billiards.jumpingrestitution--, sigma, tau
      else
	 return 0
      end
   end

   if other.isball then
      local mu_0, mu_1, v_a, v_b, p, V, mu
      
      -- The coefficient of friction for
      -- ball-ball collisions is highly
      -- dependent on velocity.

      mu_0 = billiards.slowfriction
      mu_1 = billiards.fastfriction
      p = math.scale(math.add(a.position, b.position), 0.5)
			      
      v_a = physics.pointvelocity(a, p)
      v_b = physics.pointvelocity(b, p)
      V = math.distance (v_a, v_b)
      mu = mu_1 + (mu_0 - mu_1) * math.exp(-0.77 * V)

      callhooks(billiards.ballcollision)
			      
      return 1, mu, billiards.collidingrestitution
   end
			   
   if other.iscushion then
      callhooks (billiards.cushioncollision)

      -- Nothing fancy for ball-cushion 
      -- collisions for now.
			      
      -- print (math.deg(math.atan(math.abs(cueball.position[2]) /
      -- 		       (0.5 * billiards.tablewidth))))
      -- os.exit()

      return 1, billiards.bouncingfriction, billiards.bouncingrestitution
   end

   if other.iscue and ball.isball then
      callhooks (billiards.cuecollision)

      other.step = function (self)
		      callhooks (billiards.striking)
		      self.step = nil
		   end

      -- 	 print(math.deg(math.atan (2.5 * l *
      -- 				   math.sqrt(1 - l ^ 2) /
      -- 				(1 + 75 +
      -- 				 2.5 * l ^ 2))))
			      
      return 1, billiards.strikingfriction, billiards.strikingrestitution
   end

   if other.ispocket then
      return 10, 0.2, 0.1
   end

   if other.istable then
      return 1, 0, 0.3
   end

   if other.isreturn then
      return 1, 1, 0.1, 1e-6, 0.7
   end

   if other.isfloor then
      return 1, 0.4, 0.4
   end
end
