-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

local w = billiards.tablewidth
local h = billiards.tableheight
local h_0 = billiards.cushionheight

-- The billiards table.

return options.billiards and bodies.environment {
   isenvironment = true,

   frame = resources.polyhedron "billiards/polyhedra/caromtop.lc" {
      istable = true,

      wood = options.toon and toon.cel {
	 color = math.scale({0.2156, 0.0380, 0.0019}, 0.2),
	 mesh = resources.scaled "billiards/meshes/caromtable.lc" ()
      } or shading.phong {
	 diffuse = resources.mirrored "billiards/imagery/diffuse/figured.lc",
	 specular = resources.mirrored "billiards/imagery/specular/figured.lc",
	 parameter = 32,

	 mesh = resources.scaled "billiards/meshes/caromtable.lc" (),
      },

      markers = options.toon and toon.cel {
	 color = {0.97, 0.97, 0.82},
	 mesh = resources.laidout "billiards/meshes/diamonds.lc" {
	    position = {0, 0, 1e-4},
	 }
      } or shading.phong {
	 
	 diffuse = {0.97, 0.87, 0.72},
	 specular = {0.45, 0.435 ,0.425},
	 parameter = 16,

	 mesh = resources.laidout "billiards/meshes/diamonds.lc" {
	    position = {0, 0, 1e-4},
	 }
      },
   },
   
   cloth = bodies.composite {
      -- The bed.
      
      bodies.box {
	 position = {0, 0, -0.005},
	 size = {w, h, 0.01},
	 isbed = true,
      },

      -- The cushions.

      bodies.capsule {
	 orientation = transforms.euler (0, 90, 0),
	 radius = 0.002,
	 length = w,
	 position = {0, 0.5 * h, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler (0, 90, 0),
	 radius = 0.002,
	 length = w,
	 position = {0, -0.5 * h, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler (90, 0, 0),
	 radius = 0.002,
	 length = h,
	 position = {0.5 * w, 0, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler (90, 0, 0),
	 radius = 0.002,
	 length = h,
	 position = {-0.5 * w, 0, h_0},

	 iscushion = true
      },
      
      surface = options.toon and toon.cel {
	 color = {0.84, 0.82, 0.39},
	 mesh = resources.scaled "billiards/meshes/caromcloth.lc" ()
      } or shading.oren {
	 diffuse = resources.periodic "billiards/imagery/diffuse/beigecloth.lc",
	 parameter = 3.2,
	 
	 mesh = resources.scaled "billiards/meshes/caromcloth.lc" ()
      }
   }
}
