#!/bin/sh

version=0.4.1
short="A free cue sports simulator."

long=" Billiards is a free cue sports simulator. It aims for physical\n\
 accuracy and simplicity and should hopefully be useful for practicing\n\
 billiards on your own and against your friends when a real pool table\n\
 is not available. Currently both a pool table and a billiards table\n\
 (that is with and without pockets) are implemented allowing you to\n\
 play eightball, nineball and carom billiards games."

menu='?package(billiards):needs="X11" section="Games/Simulation" title="Billiards" command="/usr/games/billiards-browser"'

rules='\noverride_dh_auto_configure:\n\tdh_auto_configure -- --prefix=/usr --bindir=/usr/games\n\noverride_dh_installinfo:\n\tdh_installinfo\n\trm -f $(CURDIR)/debian/billiards/usr/share/info/dir'

while [ -n "$1" ]; do
    if [ $1 = "-d" ]; then
	suffix=+cvs$(date +%Y%m%d)
	echo Appending suffix \'${suffix}\' to package version.
    else
	suffix=""
    fi

    shift 1
done

cd /tmp/

echo Getting current cvs snapshot.
cvs -Q -z3 -d:pserver:anonymous@cvs.savannah.nongnu.org:/sources/billiards co billiards

echo Debianizing.
mv billiards billiards-${version}${suffix}
cd billiards-${version}${suffix}
autoreconf -i
./configure --prefix=/usr --bindir=/usr/games && make dist
mv *.tar.gz ../billiards-${version}${suffix}.orig.tar.gz
echo "\n" | dh_make -c gpl -e dpapavas@gmail.com --single -f ../billiards-${version}${suffix}.orig.tar.gz

sed "s/Section: .*/Section: games/;s/Homepage: .*/Homepage: http:\/\/www.nongnu.org\/billiards\//;s/\(Build-Depends: .*\)/\1, dpkg (>= 1.15.4) | install-info, `dpkg-depcheck -d ./configure 2> /dev/null | grep "^  " | tr '\n' ',' | sed "s/,$//;s/  //g;s/,/, /g"`/;s/^Depends:/Depends: techne,/;s/Description: .*/Description: ${short}/;s/^ <insert long.*/${long}/" debian/control > debian/control~
mv debian/control~ debian/control

sed "s/<url:\/\/example\.com>/http:\/\/www.nongnu.org\/billiards\//;s/<put author's name and email here>/Dimitris Papavasiliou <dpapavas@gmail.com>/;s/<Copyright (C) YYYY Firstname Lastname>/Copyright (C) `date +%Y` Dimitris Papavasiliou/;/^ *<likewise for another author>/d;/^#.*/d" debian/copyright | awk 1 RS= ORS="\n\n" > debian/copyright~
mv debian/copyright~ debian/copyright

sed "s/^ *\* Initial.*$/  * Snapshot release./" debian/changelog > debian/changelog~
mv debian/changelog~ debian/changelog

echo ${rules} >> debian/rules

rm debian/README.*
rm debian/*.ex debian/*.EX

echo ${menu} > debian/billiards.menu

echo Building.
dpkg-buildpackage -j2 -b -rfakeroot
